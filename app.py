from flask import Flask, request
import requests
from twilio.twiml.messaging_response import MessagingResponse
import os

app = Flask(__name__)


# url = {"http://worldcup.sfg.io/matches/country?fifa_code="}
url = {'country': "http://worldcup.sfg.io/matches/country?fifa_code="}

countries = ['KOR', 'PAN', 'MEX', 'ENG', 'COL', 'JPN', 'POL', 'SEN',
'RUS', 'EGY', 'POR', 'MAR', 'URU', 'KSA', 'IRN', 'ESP',
'DEN', 'AUS', 'FRA', 'PER', 'ARG', 'CRO', 'BRA', 'CRC',
'NGA', 'ISL', 'SRB', 'SUI', 'BEL', 'TUN', 'GER', 'SWE']


# http://twilliopython.heroku.com/
@app.route('/', methods=['POST'])
def receive_sms():
    body = request.values.get("Body", None)
    response = MessagingResponse()

    # True or False >> BRA
    if body.upper() in countries:
        get_req = requests.get(url['country']+body).json()
        headline = "\n  --- Past Matches --- \n"

        for match in get_req:
            if match["status"] == "completed":
                headline += (
                    match["home_team"]["country"] + " " +
                    str(match["home_team"]["goals"]) + " vs " +
                    match["away_team"]["country"] + " " +
                    str(match["away_team"]["goals"]) + "\n"
                )
    
    response.message(headline)

    return str(response)


if __name__ == "__main__":
    port = int(os.environ.get("PORT", 5000))
    app.run(host='0.0.0.0', port=port)






'''
 --- Past Matches ---
France 4 vs Korea Republic 0
France 2 vs Norway 1
Nigeria 0 vs France 1
France 2 vs Brazil 1
France 1 vs USA 2
'''


'''
requests.exceptions.SSLError: HTTPSConnectionPool(host='worldcup.sfg.io', 
port=443): Max retries exceeded with url: /matches/country?fifa_code=bra 
(Caused by SSLError(SSLCertVerificationError(1, '[SSL: CERTIFICATE_VERIFY_FAILED] 
certificate verify failed: certificate has expired (_ssl.c:1123)')))
'''


